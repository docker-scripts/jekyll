include(focal)

### install ruby and its dependencies for native gems
RUN apt install --yes ruby ruby-dev make gcc build-essential

### install jekyll and bundler
RUN gem update --system --no-document &&\
    gem install jekyll &&\
    gem install bundler

### install apache2
RUN apt install --yes apache2 && a2enmod ssl

RUN apt install --yes git wget curl unzip
