cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds inject ubuntu-fixes.sh
    ds inject ssmtp.sh
    ds inject logwatch.sh $(hostname)
    ds inject setup-locale.sh

    # create a sample project
    if [[ ! -d projects ]]; then
        mkdir projects
        local dir=projects/proj1.example.org
        ds jekyll new $dir
        ds jekyll update $dir
        ds jekyll render $dir
        ds site add proj1.example.org $dir
    fi
}
