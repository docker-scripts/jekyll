cmd_jekyll_help() {
    cat <<_EOF
    jekyll <command> <dir>
        Run a jekyll command on the given project directory.
        The command can be one of: new, install/update, serve, render/build

_EOF
}

cmd_jekyll() {
    local cmd=$1;  shift
    local dir=$1;  shift
    [[ -z $dir ]] && echo -e "Usage:\n$(cmd_jekyll_help)\n" && exit 1

    case $cmd in
        new)
            ds exec bash -c \
               "mkdir -p $dir; cd $dir; jekyll new . --skip-bundle"
            ;;
        install|update)
            ds exec bash -c "cd $dir; bundle update --bundler"
            ds exec bash -c "cd $dir; bundle update; bundle install"
            ;;
        render|build)
            ds exec bash -c \
               "cd $dir; \
                LC_ALL=en_US.UTF-8 bundle exec jekyll build --destination www $@"
            ;;
        serve)
            ds exec bash -c \
               "cd $dir;
                LC_ALL=en_US.UTF-8 bundle exec \
                jekyll serve --host 0.0.0.0 --incremental $@"
            ;;
        *)
            echo -e "Usage:\n$(cmd_jekyll_help)\n"
            exit 1
            ;;
    esac
}
