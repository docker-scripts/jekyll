cmd_site_help() {
    cat <<_EOF
    site add <domain> <dir> [-m | --multiproject]
         Add an apache2 site configuration for a project directory.
         If the option '-m' is given, then this domain is going to host
         multiple projects.

    site del <domain>
         Delete an apache2 site.

_EOF
}

cmd_site() {
    local cmd=$1
    local domain=$2
    [[ -z $domain ]] && fail "Usage:\n$(cmd_site_help)\n"
    local dir=$3
    local multi=$4

    case $cmd in
        add) _add_site $domain $dir $multi ;;
        del) _del_site $domain ;;
        *)   fail "Usage:\n$(cmd_site_help)\n" ;;
    esac

    # update network aliases for this container
    _update_network_aliases
    
    # restart apache2
    ds exec systemctl restart apache2
}

_add_site() {
    local domain=$1 dir=$2 multi=$3
    [[ -z $dir ]] && fail "Usage:\n$(cmd_site_help)\n"
    [[ -z $multi ]] && dir+='/www'
    
    cat <<EOF > apache2/sites-available/$domain.conf
<VirtualHost *:80>
        ServerName $domain
        RedirectPermanent / https://$domain/
</VirtualHost>

<VirtualHost _default_:443>
        ServerName $domain

        DocumentRoot /host/$dir
        <Directory /host/$dir/>
            AllowOverride All
            Require all granted
        </Directory>

        SSLEngine on
        SSLCertificateFile  /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
        </FilesMatch>
</VirtualHost>
EOF
    ln -s ../sites-available/$domain.conf apache2/sites-enabled/
    
    ds @revproxy domains-add $domain
    ds @revproxy get-ssl-cert $domain
}

_del_site() {
    local domain=$1
    rm -f apache2/sites-enabled/$domain.conf
    rm -f apache2/sites-available/$domain.conf
    ds @revproxy domains-rm $domain
}

_update_network_aliases() {
    local aliases=''
    for file in $(ls apache2/sites-enabled); do
        domain=${file%.conf}
        aliases+=" --alias $domain"
    done
    docker network disconnect --force $NETWORK $CONTAINER
    docker network connect $aliases $NETWORK $CONTAINER
}
