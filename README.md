# Jekyll container

https://jekyllrb.com/

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull jekyll`

  - Create a directory for the container: `ds init jekyll @jekyll`

  - Fix the settings: `cd /var/ds/jekyll/ ; vim settings.sh`

  - Make the container: `ds make`

## Try the sample project

To look at the sample project, add `127.0.0.1 proj1.example.org` to
`/etc/hosts` then open in browser: https://proj1.example.org/

Alternatively, you can run `ds jekyll serve proj1.example.org`,
then open http://127.0.0.1:4000 in browser.

The directory of this project is: `projects/proj1.example.org`.  If
you make some changes to it, you can rebuild the site with: `ds jekyll
render projects/proj1.example.org`, or with: `ds jekyll render
projects/proj1.example.org --incremental`

To update the gems of this project, run: `ds jekyll update
projects/proj1.example.org`

## Add another project

Let's say that we want to add a new project based on the
**author-jekyll-template**. We can do it like this:

- Get the code of the template and place it on `projects/`:
  ```
  git clone https://github.com/CloudCannon/author-jekyll-template
  mv author-jekyll-template projects/
  ```
- Install the gems for the project:
  ```
  ds jekyll install projects/author-jekyll-template
  ```
- Build the content of the project:
  ```
  ds jekyll render projects/author-jekyll-template
  ```
- Add an apache2 site configuration for this project:
  ```
  ds site add proj2.example.org projects/author-jekyll-template
  ```
- Add this line in `/etc/hosts`:
  ```
  127.0.0.1 proj2.example.org
  ```
  Then open it in browser: https://proj2.example.org
- If the domain is a real one, you can get a letsencrypt SSL certificate like this:
  ```
  ds @revproxy get-ssl-cert <domain>
  ```
- To serve it locally, run: `ds jekyll serve projects/author-jekyll-template` and then open
  in browser http://127.0.0.1:4000

**Note:** If you have many projects, it might be better to name the
project the same as the domain, in order to avoid any confusion when
managing them. For example:
```
git clone https://github.com/CloudCannon/author-jekyll-template
mv author-jekyll-template projects/proj2.example.org
```

## Remove a project

If we want to remove the sample project for example, we can do it like
this:
- First delete the apache2 site: `ds site del proj1.example.org`
- Then remove the project: `rm -rf projects/proj1.example.org`

## Serve multiple projects from the same site/domain

If you want to serve multiple projects from the same site/domain, like
https://projects.example.org/proj-1/, https://projects.example.org/proj-2/,
https://projects.example.org/proj-3/, etc, you can do it like this:
```
ds jekyll new projects/sample
ds jekyll install projects/sample
ds jekyll render projects/sample

cp -a projects/sample projects/proj-1
cp -a projects/sample projects/proj-2
cp -a projects/sample projects/proj-3
rm -rf projects/sample

mkdir projects/projects.example.org
cd projects/projects.example.org/
ln -s ../proj-1/www proj-1
ln -s ../proj-2/www proj-2
ln -s ../proj-3/www proj-3
cd -

ds site add projects.example.org projects/projects.example.org --multiproject
# don't forget the --multiproject option at the end
```

You also need to edit the config file `projects/proj-1/_config.yml` and
set `baseurl: "/proj-1"`. Do the same for the other projects, and then
render (generate) the html pages again:
```
ds jekyll render projects/proj-1
ds jekyll render projects/proj-2
ds jekyll render projects/proj-3
```

## Other commands

```
ds stop
ds start
ds shell
ds help
```
